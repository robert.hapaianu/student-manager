package com.example.studentmanager.Repo;

import com.example.studentmanager.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface StudentRepo extends JpaRepository<Student, Long> {
    void deleteById(Long id);

    Optional<Student> findStudentById(Long id);
}
