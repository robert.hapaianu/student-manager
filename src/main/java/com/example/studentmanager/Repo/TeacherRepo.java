package com.example.studentmanager.Repo;

import com.example.studentmanager.model.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface TeacherRepo extends JpaRepository<Teacher, Long> {
    void deleteById(Long id);

    Optional<Teacher> findTeacherById(Long id);
}