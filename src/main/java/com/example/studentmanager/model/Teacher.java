package com.example.studentmanager.model;


import jakarta.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;

@Entity
public class Teacher implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false, updatable = false)
    private Long id;
    private String name;
    private String email;
    private String phone;
    private String imageUrl;
    private ArrayList<String> Objects;
    @Column(nullable = false, updatable = false)
    private String teacherCode;

    public Teacher(String name, String email, String phone, String imageUrl, ArrayList<String> objects, String teacherCode) {
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.imageUrl = imageUrl;
        Objects = objects;
        this.teacherCode = teacherCode;
    }

    public Teacher() {
    }

    public ArrayList<String> getObjects() {
        return Objects;
    }

    public void setObjects(ArrayList<String> objects) {
        Objects = objects;
    }

    public String getTeacherCode() {
        return teacherCode;
    }

    public void setTeacherCode(String teacherCode) {
        this.teacherCode = teacherCode;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public String toString() {
        return "Teacher{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", Objects=" + Objects +
                '}';
    }
}

