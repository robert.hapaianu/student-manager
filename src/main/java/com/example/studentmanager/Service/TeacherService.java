package com.example.studentmanager.Service;

import com.example.studentmanager.Repo.TeacherRepo;
import com.example.studentmanager.exception.UserNotFoundException;
import com.example.studentmanager.model.Teacher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class TeacherService {
    private final TeacherRepo teacherRepo;

    @Autowired
    public TeacherService(TeacherRepo teacherRepo) {
        this.teacherRepo = teacherRepo;
    }

    public Teacher  addTeacher(Teacher teacher) {
        teacher.setTeacherCode(UUID.randomUUID().toString());
        return teacherRepo.save(teacher);
    }

    public List<Teacher> findAllTeachers() {
        return teacherRepo.findAll();
    }

    public Teacher updateTeacher(Teacher teacher) {
        return teacherRepo.save(teacher);
    }

    public Teacher findTeacherById(Long Id) {
        return teacherRepo.findTeacherById(Id)
                .orElseThrow(() -> new UserNotFoundException("Teacher by id " + " was not found"));
    }

    public void deleteTeacher(Long id) {
        teacherRepo.deleteById(id);
    }

}