package com.example.studentmanager.Service;

import com.example.studentmanager.Repo.StudentRepo;
import com.example.studentmanager.exception.UserNotFoundException;
import com.example.studentmanager.model.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class StudentService {
    private final StudentRepo studentRepo;

    @Autowired
    public StudentService(StudentRepo studentRepo) {
        this.studentRepo = studentRepo;
    }

    public Student addStudent(Student student) {
        student.setStudentCode(UUID.randomUUID().toString());
        return studentRepo.save(student);
    }

    public List<Student> findAllStudents() {
        return studentRepo.findAll();
    }

    public Student updateStudent(Student student) {
        return studentRepo.save(student);
    }

    public Student findStudentById(Long Id) {
        return studentRepo.findStudentById(Id)
                .orElseThrow(() -> new UserNotFoundException("Student by id " + " was not found"));
    }

    public void deleteStudent(Long id) {
        studentRepo.deleteById(id);
    }

}
